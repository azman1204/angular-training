import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  names = ['Superman', 'Spiderman', 'Ultraman'];
  show = false;
  @Input() text = ''; // property ini boleh di inject melalui tag <app-home [text] = "....">
  @Output() title = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
  }

  doclick(name:string) {
    alert('You clicked me !' + name);
    this.title.emit();
  }

}
