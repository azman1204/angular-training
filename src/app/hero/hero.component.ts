import { Component, OnInit } from '@angular/core';

// decorator / annotation = meta data
// <app-hero></app-hero>
@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.css']
})
export class HeroComponent {
  title = 'Hello World';
  url = 'https://miro.medium.com/max/480/1*9A6E9kaZZ54idy0HLSlh-A.png';
  google = 'https://google.com';
  isLike = false;
  color = 'green';
  no = 5;
}
